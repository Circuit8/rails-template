# frozen_string_literal: true

rails_command 'tailwindcss:install'

inject_into_file 'config/tailwind.config.js',
                 "    './app/decorators/**/*.rb',\n    './app/components/**/*',\n",
                 after: "'./app/helpers/**/*.rb',\n"
