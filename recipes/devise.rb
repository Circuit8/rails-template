# frozen_string_literal: true

generate 'devise:install'
generate :devise, @authentication_model

inject_into_file "app/models/#{@authentication_model.underscore.downcase}.rb",
                 "# User class\n",
                 before: "class #{@authentication_model.classify} < ApplicationRecord"

inject_into_file "spec/factories/#{@authentication_model.underscore.downcase.pluralize}.rb",
                 "email { FFaker::Internet.email }\npassword { 'password' }\n",
                 after: "factory :#{@authentication_model.underscore.downcase} do\n"
