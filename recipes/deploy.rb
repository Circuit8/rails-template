remote_template 'gitlab-ci.yml', '.gitlab-ci.yml'
remote_template 'docker-compose.yml.erb', 'docker-compose.yml'
remote_template '.docker-compose/review.yml', '.docker-compose/review.yml'
remote_template '.docker-compose/production.yml', '.docker-compose/production.yml'

remote_template 'dockerignore', '.dockerignore'
remote_template 'Dockerfile.erb', 'Dockerfile'

create_file '.gitlab/README.md' do
  <<-README
  # GitLab CI Jobs
  #
  # Put your custom Gitlab CI jobs here.
  # Make sure they are named with a .yml extension.
  #
  README
end
