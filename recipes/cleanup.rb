# frozen_string_literal: true

gsub_file 'app/views/layouts/application.html.erb', /^\s+$/, ''

run 'bin/rubocop -A > /dev/null || true'
