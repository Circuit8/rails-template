# frozen_string_literal: true

gsub_file 'Gemfile', /gem\s+["']sass-rails["'].*$/, "gem 'sassc-rails'"
gsub_file 'Gemfile', /ruby\s+["']([0-9.]+)["']/, 'ruby \'~> \1\''
gsub_file 'Gemfile', /# gem\s+["']image_processing["'].*$/, "gem 'image_processing'"
gsub_file 'Gemfile', /# gem\s+["']bcrypt["'].*$/, "gem 'bcrypt'"

gem 'config'
if @devise
  gem 'devise'
  gem 'devise-i18n'
  gem 'devise_invitable'
end
gem 'draper'
gem 'font_awesome5_rails'
gem 'heroicon'
gem 'kaminari'
gem 'postmark-rails' if @postmark
gem 'pundit'
gem 'rails-i18n'
if @sentry
  gem 'sentry-ruby'

  gem 'sentry-rails'
  gem 'sentry-sidekiq'
end
gem 'sidekiq'
gem 'tailwindcss-rails'
gem 'view_component'

gem_group :development do
  gem 'annotate'
end

gem_group :test do
  gem 'capybara-email'
  gem 'cuprite'
  gem 'launchy'
  gem 'shoulda-matchers'
  gem 'simplecov', require: false
  gem 'timecop'
  gem 'vcr'
  gem 'webmock'
end

gem_group :development, :test do
  gem 'factory_bot_rails'
  gem 'ffaker'
  gem 'pry-rails'
  gem 'rspec-rails'
  gem 'rubocop', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
  gem 'rubocop-performance', require: false
end
