# frozen_string_literal: true

system 'bin/importmap pin stimulus-reveal stimulus-data-bindings stimulus-existence'

remove_file 'app/javascript/controllers/hello_controller.js'

append_to_file 'app/javascript/controllers/index.js', <<~JS

  import RevealController from 'stimulus-reveal'
  application.register('reveal', RevealController)
  import ExistenceController from 'stimulus-existence'
  application.register('existence', ExistenceController)
  import DataBindingController from 'stimulus-data-bindings'
  application.register('data-binding', DataBindingController)
JS
