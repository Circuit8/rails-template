# frozen_string_literal: true

say('=====================================================================')
say('You have installed a new Rails app')
git_url = @git_repo.gsub(':', '/').gsub('git@', 'https://').gsub('.git', '')
say('You can access the repository at:')
say(git_url, :green)
say('')
say('Please follow the instructions below before making your first commit:')
say('')
say('Sentry', :yellow)
say('* Run `rails credentials:edit` and add the following line:')
say("sentry_dsn: #{@sentry_dsn}", :green)
say('')
if @devise
  say('Devise', :yellow)
  say('* change config.mailer_sender in config/initializers/devise.rb')
  say('')
end
say('Deployment', :yellow)
say('* Configure `docker-compose.yml` and `.docker-compose/*.yml` appropriately')
say('')
say('* Add the following variables to Gitlab CI:')
say('  RAILS_MASTER_KEY: ', :green)
say(`cat config/master.key`, :yellow)
say('  PRODUCTION_URL:   ', :green)
say('Ask your server administrator')
say('=====================================================================')
