# frozen_string_literal: true

binstubs = %w[puma rubocop rspec-core]
binstubs << 'sidekiq' if @sidekiq

run "bundle binstubs #{binstubs.join(' ')}"
