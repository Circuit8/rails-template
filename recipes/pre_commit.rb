# frozen_string_literal: true

@pre_commit_installed = command?('pre-commit')

if !@pre_commit_installed && default_yes?('Would you like to install pre-commit?')
  system(if OS.mac?
           'brew install pre-commit'
         elsif OS.linux?
           'sudo apt install -y python3-pip && pip install pre-commit'
         end)
end

if @pre_commit_installed
  @global_pre_commit_installed = system('git config init.templateDir')
  if !@global_pre_commit_installed && default_yes?('Install pre-commit hooks globally?')
    system 'git config --global init.templateDir ~/.git-template'
    system 'pre-commit init-templatedir ~/.git-template'
    system 'git init'
  elsif !@global_pre_commit_installed && default_yes?('Install pre-commit hooks locally?')
    system 'pre-commit install'
  end

  remote_template '.pre-commit-config.yaml'
end
