# frozen_string_literal: true

rails_command 'db:create db:migrate' if default_yes?('Set up database?')
