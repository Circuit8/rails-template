# frozen_string_literal: true

# remote_template 'eslintrc.json', '.eslintrc.json'
remote_template 'prettierrc.json', '.prettierrc.json'
remote_template 'rubocop.yml', '.rubocop.yml'
