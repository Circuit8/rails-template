# frozen_string_literal: true

generate 'rspec:install'

gsub_file 'spec/spec_helper.rb', /=(begin|end)/, ''

gsub_file 'spec/rails_helper.rb', /# (Dir\[Rails\.root\.join.*require.*)/, '\1'

remote_template 'simplecov', '.simplecov'

inject_into_file 'spec/rails_helper.rb',
                 "\nrequire 'simplecov'\n",
                 before: "require 'spec_helper'"

inject_into_file 'spec/rails_helper.rb',
                 "\nrequire 'capybara/email/rspec'",
                 after: /# Add additional requires.*/

remote_template 'spec/support/better_rails.rb'
remote_template 'spec/support/capybara.rb'
remote_template 'spec/support/cuprite.rb'
remote_template 'spec/support/devise.rb' if @devise
remote_template 'spec/support/factory_bot.rb'
remote_template 'spec/support/system_tests.rb'
remote_template 'spec/support/vcr.rb'
remote_template 'spec/support/webmock.rb'
remote_template 'spec/support/helpers/browserless_helpers.rb'
remote_template 'spec/support/helpers/cuprite_helpers.rb'
remote_template 'lib/docker_helpers.rb'
remote_template 'spec/support/helpers/docker_helpers.rb'

append_to_file '.gitignore', <<~GITIGNORE
  # Ignore RSpec results
  spec/examples.txt

  # Ignore simplecov output
  coverage
GITIGNORE
