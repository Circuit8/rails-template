add_file 'app/components/application_component.rb' do
  <<~RB
    # Application Component
    class ApplicationComponent < ViewComponent::Base; end
  RB
end
