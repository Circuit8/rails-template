# frozen_string_literal: true

git :init
git add: '.'
git commit: "-m 'Initial commit'"
git remote: "add origin #{@git_repo}"
