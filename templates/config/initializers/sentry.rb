# frozen_string_literal: true

Sentry.init do |config|
  config.dsn = ENV.fetch('SENTRY_DSN', Rails.application.credentials.sentry_dsn)
  config.breadcrumbs_logger = %i[sentry_logger active_support_logger http_logger]
  config.enabled_environments = %w[review production]

  # Performance info
  # https://docs.sentry.io/platforms/ruby/configuration/sampling/#configuring-the-transaction-sample-rate
  config.traces_sampler = lambda do |sampling_context|
    # if this is the continuation of a trace, just use that decision (rate controlled by the caller)
    next sampling_context[:parent_sampled] unless sampling_context[:parent_sampled].nil?

    # transaction_context is the transaction object in hash form
    # keep in mind that sampling happens right after the transaction is initialized
    # for example, at the beginning of the request
    transaction_context = sampling_context[:transaction_context]

    # transaction_context helps you sample transactions with more sophistication
    # for example, you can provide different sample rates based on the operation or name
    op = transaction_context[:op]

    case op
    when /request/
      0.5
    when /sidekiq/
      0.1
    else
      0.0
    end
  end
end
