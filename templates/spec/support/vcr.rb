# frozen_string_literal: true

VCR.configure do |config|
  config.hook_into :webmock
  config.ignore_hosts 'chrome', REMOTE_CHROME_HOST, '0.0.0.0'
  config.allow_http_connections_when_no_cassette = true
  config.configure_rspec_metadata!
  config.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
end
