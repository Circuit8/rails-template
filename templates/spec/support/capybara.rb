# frozen_string_literal: true

# spec/system/support/capybara_setup.rb

Capybara.server = :puma, { Silent: true, queue_requests: true }

# Usually, especially when using Selenium, developers tend to increase the max wait time.
# With Cuprite, there is no need for that.
# We use a Capybara default value here explicitly.
Capybara.default_max_wait_time = 2

# Enable finding elements by ARIA labels.
Capybara.enable_aria_label = true

# Normalize whitespaces when using `has_text?` and similar matchers,
# i.e., ignore newlines, trailing spaces, etc.
# That makes tests less dependent on slightly UI changes.
Capybara.default_normalize_ws = true

# Where to store system tests artifacts (e.g. screenshots, downloaded files, etc.).
# It could be useful to be able to configure this path from the outside (e.g., on CI).
# Capybara.save_path = ENV.fetch('CAPYBARA_ARTIFACTS', './tmp')

# Use a hostname that could be resolved in the internal Docker network
# In Rails 6.1+ the following line should be enough
CURRENT_HOSTNAME = (
  ENV['REMOTE_CONTAINERS'] ? `hostname -f` : `ip route | awk '/scope/' | cut -f 8 -d " "`
)&.strip&.downcase

# Make server accessible from the outside world
Capybara.server_port = 22_728 + ENV['TEST_ENV_NUMBER'].to_i
Capybara.server_host = CURRENT_HOSTNAME.delete("\n")
Capybara.app_host = "http://#{CURRENT_HOSTNAME.delete("\n") || '0.0.0.0'}"

Capybara.singleton_class.prepend(Module.new do
  attr_accessor :last_used_session

  def using_session(name, &block)
    self.last_used_session = name
    super
  ensure
    self.last_used_session = nil
  end
end)
